const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');
const {InvalidCredentialsError} = require('../utils/customErrors');

const getUser = async (req, res) => {
  const {userId} = req.user;
  const user = await User.findOne({_id: userId}, '-__v -password');

  if (!user) {
    throw new InvalidCredentialsError('No user found');
  }

  res.status(200).json({user});
};

const deleteUser = async (req, res) => {
  const {userId} = req.user;
  const user = await User.findOneAndRemove({_id: userId});

  if (!user) {
    throw new InvalidCredentialsError('No user found');
  }

  res.status(200).json({message: 'Success'});
};

const changeUserPass = async (req, res) => {
  const {userId} = req.user;
  const {oldPassword, newPassword} = req.body;
  const user = await User.findOne({_id: userId});

  if (!user) {
    throw new InvalidCredentialsError('No user found');
  }

  if (!oldPassword || !newPassword) {
    throw new InvalidCredentialsError('Please, provide old and new password');
  }

  if (oldPassword === newPassword) {
    throw new InvalidCredentialsError(`
      New password must be different from old password
    `);
  }

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new InvalidCredentialsError('Old password is not correct');
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();

  res.status(200).json({message: 'Success'});
};

module.exports = {
  getUser,
  deleteUser,
  changeUserPass,
};
