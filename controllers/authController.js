const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {User} = require('../models/userModel');
const {InvalidCredentialsError} = require('../utils/customErrors');
const {JWT_SECRET} = require('../config');

const registerUser = async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();
  res.status(200).json({message: 'Success'});
};

const loginUser = async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  const user = await User.findOne({username});

  if (!user) {
    throw new InvalidCredentialsError();
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new InvalidCredentialsError();
  }

  const token = jwt.sign({
    _id: user._id,
    username: user.username,
  }, JWT_SECRET);

  res.status(200).json({
    message: 'Success',
    jwt_token: token,
  });
};

module.exports = {
  registerUser,
  loginUser,
};
