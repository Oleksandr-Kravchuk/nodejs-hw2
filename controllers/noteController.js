const {Note} = require('../models/noteModel');
const {InvalidRequestError} = require('../utils/customErrors');

const getNotes = async (req, res) => {
  const {userId} = req.user;
  const offset = Number(req.query.offset) || 0;
  const limit = Number(req.query.limit) || 0;
  const count = await Note.find({userId}).countDocuments();
  const notes = await Note.find({userId}, '-__v').skip(offset).limit(limit);

  if (!notes) {
    throw new InvalidRequestError('No note with such id found');
  }

  res.status(200).json({
    offset,
    limit,
    count,
    notes,
  });
};

const getNoteById = async (req, res) => {
  const {userId} = req.user;
  const noteId = req.params.id;
  const note = await Note.findOne({_id: noteId, userId: userId}, '-__v');

  if (!note) {
    throw new InvalidRequestError('No note with such id found');
  }

  res.status(200).json({note});
};

const addNote = async (req, res) => {
  const {userId} = req.user;
  const note = new Note({...req.body, userId});

  await note.save();
  res.json({message: 'Note created successfully'});
};

const checkUncheckNoteById = async (req, res) => {
  const {userId} = req.user;
  const noteId = req.params.id;
  const note = await Note.findOne({_id: noteId, userId: userId});

  if (!note) {
    throw new InvalidRequestError('No note with such id found');
  }

  note.completed = !note.completed;
  await note.save();

  res.status(200).json({message: 'Note check/uncheck successfully'});
};

const updateNoteById = async (req, res) => {
  const {userId} = req.user;
  const noteId = req.params.id;
  const text = req.body;
  const note = await Note.findOne({_id: noteId, userId: userId});

  if (!note) {
    throw new InvalidRequestError('No note with such id found');
  }

  await Note.findOneAndUpdate({
    _id: noteId, userId: userId}, {$set: text});

  res.status(200).json({message: 'Note update successfully'});
};

const deleteNoteById = async (req, res) => {
  const {userId} = req.user;
  const noteId = req.params.id;
  const note = await Note.findOne({_id: noteId, userId: userId});

  if (!note) {
    throw new InvalidRequestError('No note with such id found');
  }

  await Note.findOneAndRemove({_id: noteId, userId: userId});

  res.status(200).json({message: 'Note delete successfully'});
};

module.exports = {
  getNotes,
  getNoteById,
  addNote,
  checkUncheckNoteById,
  updateNoteById,
  deleteNoteById,
};
