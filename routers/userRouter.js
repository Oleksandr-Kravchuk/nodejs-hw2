const express = require('express');
const router = new express.Router();
const {
  getUser,
  deleteUser,
  changeUserPass,
} = require('../controllers/userController');
const {asyncWrapper} = require('../utils/asyncWrapper');

router.get('/', asyncWrapper(getUser));
router.delete('/', asyncWrapper(deleteUser));
router.patch('/', asyncWrapper(changeUserPass));

module.exports = {
  userRouter: router,
};
