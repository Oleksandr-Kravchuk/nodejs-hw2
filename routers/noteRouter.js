const express = require('express');
const router = new express.Router();
const {
  getNotes,
  getNoteById,
  addNote,
  checkUncheckNoteById,
  updateNoteById,
  deleteNoteById,
} = require('../controllers/noteController');
const {asyncWrapper} = require('../utils/asyncWrapper');
const {noteValidator} = require('../middlewares/validationMiddleware');

router.get('/', asyncWrapper(getNotes));
router.get('/:id', asyncWrapper(getNoteById));
router.post('/', noteValidator, asyncWrapper(addNote));
router.patch('/:id', asyncWrapper(checkUncheckNoteById));
router.put('/:id', noteValidator, asyncWrapper(updateNoteById));
router.delete('/:id', asyncWrapper(deleteNoteById));


module.exports = {
  noteRouter: router,
};
